package com.fisglobal.lab.person;

public class PersonMain {

    public static void main(String[] args) {

        Person kowalski = new Person(3734573273547365L, "Jan", "Kowalski");
        //kowalski.lastName = "Kowalski";
        //kowalski.firstName = "Jan";
        //kowalski.pesel = 3734573273547365L;

        System.out.println("1) last name: " + kowalski);

        kowalski = null;

        long p = 6543265465463L;
        Person nowak = new Person(p);

        nowak.setFirstName("Adam");
        nowak.setLastName("Nowak");
        nowak.setPesel(6543265465463L);

        System.out.println("2) last name: " + nowak.getPesel() + " pesel: " + nowak.getPesel());
    }
}
