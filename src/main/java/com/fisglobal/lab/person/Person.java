package com.fisglobal.lab.person;

// JavaBeans
public class Person {

    private long pesel;
    private String firstName;
    private String lastName;

    public Person(long pesel, String firstName, String lastName) {
        this.pesel = pesel;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Person(long pesel){
        this.pesel = pesel;
    }

    public Person(){
    }

    public long getPesel(){
        return this.pesel;
    }

    public void setPesel(long pesel){
        this.pesel = pesel;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder("Person: ");

        builder.append("pesel=").append(pesel).append(", ");
        builder.append("first name=").append(firstName).append(", ");
        builder.append("last name=").append(lastName);

        return builder.toString();
        /*"Person{" +
                "pesel=" + pesel +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';*/
    }
}
