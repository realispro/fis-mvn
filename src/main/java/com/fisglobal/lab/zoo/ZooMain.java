package com.fisglobal.lab.zoo;

import com.fisglobal.lab.calc.ArrayMemory;
import com.fisglobal.lab.zoo.animals.Eagle;
import com.fisglobal.lab.zoo.animals.Kiwi;
import com.fisglobal.lab.zoo.animals.Shark;
import com.fisglobal.lab.zoo.animals.Stingray;

import java.util.*;

public class ZooMain {

    public static void main(String[] args) throws Exception {
        System.out.println("Let's visit zoo!");

        List<Animal> animals = new LinkedList<>();
                //new ArrayList<>();

        Animal owsiak = new Kiwi("Owsiak", 5);

        animals.add(new Stingray("Jason", 40));
        animals.add(owsiak);
        animals.add(new Shark("Stefan", 140));
        animals.add(new Eagle("Stefan", 15));
        animals.add(owsiak);

        System.out.println("animals = " + animals);
        System.out.println("animals count: " + animals.size());

        //animals.remove(owsiak);
        /*for( Animal toRemove : animals ){
            if(toRemove.getMass()<=10){
                animals.remove(toRemove);
            }
        }*/

        Iterator<Animal> iterator = animals.iterator();
        while(iterator.hasNext()){
            Animal toRemove = iterator.next();
            if(toRemove.getMass()<=10){
                iterator.remove();
            }
        }



        Comparator<Animal> comparator = new AnimalComparator();
        Collections.sort(animals, comparator);



        for( Animal a : animals ){
            System.out.println("animal = " + a);
        }

        Animal[] animalsArray = new Animal[3];
        animalsArray[0] = owsiak;
        animalsArray[1] = new Eagle("George", 14);
        animalsArray[2] = new Shark("Steven", 20);
        Arrays.sort(animalsArray, comparator);

        Animal animal = animals.get(0);
                //getAnimal();
        animal.eat("grass");
        animal.move();

        System.out.println("done.");
    }

    public static Animal getAnimal(){
        return new Kiwi("Kiwi", 2);
                // new Shark("Nemo", 500);
    }
}
