package com.fisglobal.lab.zoo;

import com.fisglobal.lab.zoo.animals.Kiwi;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class DatabaseMain {

    private static final Logger log = LogManager.getLogger(DatabaseMain.class);

    public static void main(String[] args) {
        log.info("Let's communicate with DB!");


        try(Connection connection = getConnection();){ // try-with-resources
            log.info("db version {}",
                    connection.getMetaData().getDatabaseMajorVersion());

            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery("select id, name, mass, type from animal");

            while(rs.next()){

                int id = rs.getInt("id");
                String name = rs.getString("name");
                int mass = rs.getInt("mass");
                String type = rs.getString("type");

                log.info("animal row: {}, {}, {}, {}", id, name, mass, type);
            }

        } catch (SQLException e) {
            log.error("problem while communicating with DB", e);
            throw new RuntimeException(e);
        }


        try(Connection connection = getConnection()){

            Animal animal = new Kiwi("But ozywi", 191);

            connection.setAutoCommit(false);

            PreparedStatement st =
                    connection.prepareStatement("INSERT INTO ANIMAL(NAME, MASS, TYPE) VALUES(?,?,?)");
            st.setString(1, animal.getName());
            st.setInt(2, animal.getMass());
            st.setString(3, animal.getClass().getSimpleName());

            int rows = st.executeUpdate();
            log.info("{} rows modified", rows);

            connection.commit();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        while(true){}

        //log.info("done.");
    }

    public static Connection getConnection(){

        Properties props = new Properties();
        try {
            props.load(DatabaseMain.class.getResourceAsStream("/jdbc.properties"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        String driver = props.getProperty("jdbc.driver");
        String url = props.getProperty("jdbc.url");
        String user = props.getProperty("jdbc.user");
        String password = props.getProperty("jdbc.password");

        Connection connection = null;
        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException | ClassNotFoundException e) {
            log.error("problem while obtaining connection", e);
            return null;
        }

        return connection;

    }
}
