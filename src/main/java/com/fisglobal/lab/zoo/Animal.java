package com.fisglobal.lab.zoo;

import java.util.Objects;

public abstract class Animal /*implements Comparable<Animal>*/{

    private String name;
    private int mass;

    public Animal(String name, int mass) {
        this.name = name;
        this.mass = mass;
    }

    public void eat(String food) throws Exception {
        System.out.println(this + " is eating " + food);
    }

    public abstract void move();


    public String getName() {
        return name;
    }

    public int getMass() {
        return mass;
    }

/*    @Override
    public int compareTo(Animal a) {
        return a.mass - this.mass;
    }*/

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", mass=" + mass +
                '}';
    }
}
