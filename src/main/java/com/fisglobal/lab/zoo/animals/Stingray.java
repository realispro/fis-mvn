package com.fisglobal.lab.zoo.animals;

import com.fisglobal.lab.zoo.Fish;

public class Stingray extends Fish {
    public Stingray(String name, int mass) {
        super(name, mass);
    }
}
