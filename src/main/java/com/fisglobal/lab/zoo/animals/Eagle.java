package com.fisglobal.lab.zoo.animals;

import com.fisglobal.lab.zoo.Bird;

public class Eagle extends Bird {
    public Eagle(String name, int mass) {
        super(name, mass);
    }
}
