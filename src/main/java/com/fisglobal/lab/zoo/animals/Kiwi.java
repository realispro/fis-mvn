package com.fisglobal.lab.zoo.animals;

import com.fisglobal.lab.zoo.Bird;

public class Kiwi extends Bird {
    public Kiwi(String name, int mass) {
        super(name, mass);
    }

    @Override
    public void move() {
        System.out.println(this + " is walking cause kiwi can not fly");
    }

    @Override
    public void eat(String food) throws Exception {
        if(food.equals("oat")){
            //System.out.println("bleeee");
            throw new Exception("oat is awful");
        } else {
            super.eat(food);
        }
    }
}
