package com.fisglobal.lab.zoo.animals;

import com.fisglobal.lab.zoo.Fish;

public class Shark extends Fish {
    public Shark(String name, int mass) {
        super(name, mass);
    }
}
