package com.fisglobal.lab.zoo;

public abstract class Fish extends Animal{

    public Fish(String name, int mass) {
        super(name, mass);
    }

    @Override
    public final void move() {
        System.out.println(this + " is swimming");
    }
}
