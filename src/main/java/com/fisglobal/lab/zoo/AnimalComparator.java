package com.fisglobal.lab.zoo;

import java.util.Comparator;

public class AnimalComparator implements Comparator<Animal> {
    @Override
    public int compare(Animal a1, Animal a2) {
        int diff = a1.getName().compareTo(a2.getName());
        if(diff==0){
            diff = a1.getMass() - a2.getMass();
        }
        System.out.println("comparing " + a1 + " and " + a2 + " diff: " + diff);
        return diff;
    }
}
