package com.fisglobal.lab;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringMain {

    public static void main(String[] args) {
        System.out.println("Let's process string!");

        String s1 = "abc";
        String s2 = new String("abc");
        String s3 = "  abc  ";

        System.out.println("s1==s2: " + (s1==s2));
        System.out.println("s1==s3: " + (s1==s3));
        System.out.println("s2==s3: " + (s2==s3));

        System.out.println("s1.equals(s2): " + s1.equals(s2));
        System.out.println("s1.equals(s3): " + s1.equals(s3));
        System.out.println("s2.equals(s3): " + s2.equals(s3));

        System.out.println("[" + s3.trim() + "]");
        System.out.println("substring: [" + s3.substring(2, 5) + "]");

        System.out.println("charAt: " + s3.charAt(2));
        System.out.println("indexOf: " + s3.indexOf('a'));

        String names = "Kowal Kowalski Kowalewski";
        String[] namesArray = names.split(" ");
        for( String name : namesArray){
            System.out.println("[" + name + "]");
        }

        // regex
        String text = "00-950 Warszawa, 30 512 Kraków, 88100 Toruń";
        String patternText = "\\d{2}[\\s-]?\\d{3}";

        Pattern pattern = Pattern.compile(patternText);
        Matcher matcher = pattern.matcher(text);

        System.out.println("matcher = " + matcher);
        
        while(matcher.find()){
            System.out.println("start: "  + matcher.start() + " end: " + matcher.end()
                    + " group: " + matcher.group());
        }
        
        double value = 1234.56789;

        Locale locale = //Locale.forLanguageTag("pl");
                new Locale("it", "IT");
                //Locale.CANADA_FRENCH;
                //Locale.getDefault();

        // number formatting
        NumberFormat numberFormat = NumberFormat
                .getCurrencyInstance(locale);
                //.getPercentInstance();
                //        .getInstance();
        //numberFormat.setMaximumFractionDigits(2);
        numberFormat.setMinimumFractionDigits(20);
        String valueString = numberFormat.format(value);
                // Double.toHexString(value);
                //"" + value;

        System.out.println("valueString = " + valueString);

        // date - legacy
        Date date = new Date();
        long millis = date.getTime();

        Calendar calendar = Calendar.getInstance();
        //calendar.setTime(date);
        calendar.set(2023, 4,18);
        calendar.add(Calendar.MONTH, 10);
        date = calendar.getTime();

        DateFormat dateFormat =
                new SimpleDateFormat("yyyy#MM#dd w W hh#mm#ss E X", locale);
                //DateFormat
                //.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
                //.getTimeInstance();
                //.getDateInstance();
                //.getInstance();

        String dateString = dateFormat.format(date);
        System.out.println("dateString = " + dateString);

        // date - java.time

        LocalDate ld = LocalDate.now();
        LocalTime lt = LocalTime.now();
        LocalDateTime ldt = LocalDateTime
                .of(2023, 5, 18, 14, 8)
                .plusMonths(10);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy@MM@dd w W hh@mm@ss E");

        dateString = formatter.format(ldt);
        System.out.println("[java.time]dateString = " + dateString);

        LocalDateTime localStart = LocalDateTime.now();
        ZonedDateTime warsawStart = ZonedDateTime.of(localStart, ZoneId.of("Europe/Warsaw"));
        ZonedDateTime warsawLanding = warsawStart.plusHours(7);
        ZonedDateTime singaporeLanding = warsawLanding.withZoneSameInstant(ZoneId.of("Asia/Singapore"));

        System.out.println("landing in Singapore at: " + singaporeLanding);



        System.out.println("done.");
    }
}
