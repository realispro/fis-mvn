package com.fisglobal.lab.calc;

import java.util.Arrays;

public class ArrayMemory implements Memory{

    private double[] values = new double[5];
    // [0, 0, 0, 0, 0] + 1
    // [1, 0, 0, 0, 0] + 2
    // [1, 2, 0, 0, 0]
    private int index = -1;

    @Override
    public double getValue() {
        return values[index];
    }

    @Override
    public void setValue(double value) {
        this.index = index + 1;
        this.values[index] = value;
    }

    @Override
    public void reset() {

    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ArrayMemory{");
        sb.append("values=").append(Arrays.toString(values));
        sb.append(", index=").append(index);
        sb.append('}');
        return sb.toString();
    }
}
