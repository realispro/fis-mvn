package com.fisglobal.lab.calc;

public class SimpleMemory implements Memory{

    private double value;

    @Override
    public double getValue() {
        return value;
    }

    @Override
    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public void reset() {
        value = 0;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SimpleMemory{");
        sb.append("value=").append(value);
        sb.append('}');
        return sb.toString();
    }
}
