package com.fisglobal.lab.calc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CalculatorMain {

    private static final Logger log = LogManager.getLogger(CalculatorMain.class);

    public static void main(String[] args) {
        //System.out.println("Let's calculate!");
        log.info("Let's calculate");

        Calculator c1 = new BetterCalculator();
        Calculator c2 = new BetterCalculator();

        c1.add(2);
        c1.multiply(6);
        c1.reset();
        c1.subtract(1);
        c1.divide(3);
        //c1.power(3);
        log.debug("c1 producer: " + c1.getProducer());
        log.info("c1 = " + c1.toString());
        log.warn("c2 = {}", c2);

        //System.out.println("done.");
        log.info("done.");
    }
}
