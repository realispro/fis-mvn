package com.fisglobal.lab.calc;

public interface Memory {

    double getValue();

    void setValue(double value);

    void reset();
}
