package com.fisglobal.lab.calc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BetterCalculator extends Calculator{

    private static final Logger log = LogManager.getLogger(BetterCalculator.class);


    public BetterCalculator(){
        super(0);
        log.debug("BetterCalculator: no param constructor");
    }

    public void power(int operand){
        memory.setValue(Math.pow(memory.getValue(), operand));
    }

    @Override
    public String getProducer() {
        return "Texas Instruments";
    }
}
