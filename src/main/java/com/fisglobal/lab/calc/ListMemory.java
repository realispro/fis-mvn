package com.fisglobal.lab.calc;

import java.util.ArrayList;
import java.util.List;

public class ListMemory implements Memory{

    private List<Double> listMemory = new ArrayList<>();

    @Override
    public double getValue() {
        return listMemory.get(listMemory.size()-1);
    }

    @Override
    public void setValue(double value) {
        listMemory.add(value);
    }

    @Override
    public void reset() {
        listMemory.clear();
        listMemory.add(0.0);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ListMemory{");
        sb.append("listMemory=").append(listMemory);
        sb.append('}');
        return sb.toString();
    }
}
