package com.fisglobal.lab.calc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class Calculator /*extends Object*/{

    private static final Logger log = LogManager.getLogger(Calculator.class);

    // fields
    protected Memory memory = new ListMemory();

    // constructors
    public Calculator(double result) {
        //super();
        log.debug("Calculator: param constructor " + result);
        this.memory.setValue(result);
    }

    /*public Calculator(){
    }*/

    // business methods
    public abstract String getProducer();

    public void add(double operand){
        memory.setValue( memory.getValue() + operand );
    }

    public void subtract(double operand){
        memory.setValue( memory.getValue() - operand );
    }

    public void multiply(double operand){
        memory.setValue( memory.getValue() * operand );
    }

    public void divide(double operand){
        memory.setValue( memory.getValue() / operand );
    }


    public void reset(){
        memory.reset();
    }

    // technical methods
    public double getResult() {
        return memory.getValue();
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Calculator{");
        sb.append("memory=").append(memory);
        sb.append('}');
        return sb.toString();
    }

    public static int multiply(int a, int b){
        return a*b;
    }

    public static int max(int[] abc){
        int max = 0;
        for( int candidate : abc ){
            if(candidate>max){
                max = candidate;
            }
        }
        return max;
    }

    public static double average(int[] xyz){
        double sum = 0;
        for(int value : xyz){
            sum += value;
        }
        double avg = sum / xyz.length;
        return avg;
    }

    public static int factorial(int operand){
        int factorial = 1;
        for(int i=1; i<=operand; i++ ){
            factorial = factorial * i;
        }
        return factorial;
    }




}
