package com.fisglobal.lab;


import com.fisglobal.lab.calc.Calculator;

import java.util.Arrays;

public class HelloWorld { // PascalCase, UpperCamelCase

    public static void main(String[] args) {
        System.out.println("Hey FIS! Its Tuesday"); // sout

        long age = 2_147_483_648L;
        //age = 123;

        System.out.println("age = " + age); //soutv

        float price = 123.456F;
        System.out.println("price = " + price);

        char c = 'H';
        //72;
        System.out.println("c = " + (short) c);

        boolean expensive = price > 100;
        System.out.println("expensive = " + expensive);

        int[] values = /*new int[]*/ {1, 2, 3, 4, 5};

        System.out.println("Arrays.toString(values) = " + Arrays.toString(values));

        for (int i = 0; i < values.length; i++) {
            System.out.println("values[" + i + "]=" + values[i]);
        }

        for( int x : values ){
            System.out.println("values[?]=" + x);
        }
        
        WeekDay weekDay = WeekDay.FRIDAY; // camelCase
        int weekDayOffset = weekDay.ordinal() + 1;
        System.out.println("weekDay = " + weekDayOffset );

        boolean weekend;
        if(weekDay==WeekDay.SATURDAY || weekDay==WeekDay.SUNDAY) {
            System.out.println("Weekend :)");
            weekend = true;
        } else if (weekDay==WeekDay.FRIDAY) {
            System.out.println("not sure :/");
            weekend = true;
        } else {
            System.out.println("working day :(");
            weekend = false;
        }
        // ternary operator
        weekend = weekDay==WeekDay.FRIDAY||weekDay==WeekDay.SATURDAY||weekDay==WeekDay.SUNDAY ? true : false;


        switch (weekDay){
            case FRIDAY:
                System.out.println("not sure :(");
                break;
            case SATURDAY:
            case SUNDAY:
                System.out.println("weekend :)");
                break;
            default:
                System.out.println("working day");
        }


        int size = args.length>0 ? Integer.parseInt(args[0]) : 10;
        int[][] results = new int[size][size];

        OUTER:
        for(int x=0; x<size; x++){
            for(int y=0; y<size; y++){
                int a = x+1;
                int b = y+1;
                int d = Calculator.multiply(a, b);
                results[x][y] = d;//(x+1)*(y+1);
            }
        }

        for( int[] row : results){
            for( int value : row ){
                System.out.print(value + "\t");
            }
            System.out.println();
        }


        Integer x = null;

        try {

            System.out.println("division: " + (10 / x));
        } catch(ArithmeticException | NullPointerException e){
            e.printStackTrace();
            System.err.println("pamietaj .... nie dziel przez zero: " + e.getMessage());
            throw e;
        } catch (Exception e){
            e.printStackTrace();
            System.err.println("Some exception: " + e.getMessage());
        } finally {
            System.out.println("from finally block");
        }

        System.out.println("done." + multiply(2, 2));
    }

    public static int multiply(int a, int b){
        return a*b;
    }

    // create new class
    // create main method
    // edit run configuration
    // create int array same size like args
    // iterate over args
    // convert string to int
    // assign int to int array



}
