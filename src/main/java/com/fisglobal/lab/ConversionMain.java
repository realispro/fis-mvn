package com.fisglobal.lab;

//import com.fisglobal.lab.calc.Calculator;

import java.util.Arrays;

import static com.fisglobal.lab.calc.Calculator.max;

public class ConversionMain {

    public static void main(String[] args) {

        int[] ints = new int[args.length];

        for(int index=0; index<args.length; index++ ){
            String s = args[index];
            int i = Integer.parseInt(s);
            ints[index] = i;
        }

        String intsDisplay = Arrays.toString(ints);
        System.out.println("ints = " + intsDisplay);

        // max
        int max = max(ints);
        System.out.println("max = " + max);


        // average
        // 1. sum all values
        // 2. divide by length


        double average =  com.fisglobal.lab.calc.Calculator.average(ints);
        System.out.println("average = " + average);

        // factorial
        int factorialOperand = 5;
        // 1*2*3*4*5
        // f: 0 = 1
        // f: 1 = 1
        // f: 2 = 1*2
        int factorial =  com.fisglobal.lab.calc.Calculator.factorial(5);
        System.out.println("factorial = " + factorial);



    }

    /*
    public static int max(int[] abc){
        int max = 0;
        for( int candidate : abc ){
            if(candidate>max){
                max = candidate;
            }
        }
        return max;
    }
     */

}
