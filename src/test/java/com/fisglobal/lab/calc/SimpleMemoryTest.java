package com.fisglobal.lab.calc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleMemoryTest {

    @Test
    void shouldReturnExpectedValue(){
        SimpleMemory memory = new SimpleMemory();
        memory.setValue(13);
        double value = memory.getValue();
        Assertions.assertEquals(13, value);
    }

}